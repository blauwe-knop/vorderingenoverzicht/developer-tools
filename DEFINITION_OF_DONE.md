# DEFINITION OF DONE

- All acceptance criteria are met
- All code is merged to master via an MR
- All automated tests pass (tests, code quality, linting etc.)
- All documentation is updated
- Latest product iteration is manually tested
- Product Owner accepts the result

## ADDITIONAL GOALS (not part of DoD for now)

- Latest product is deployed to app store/demo environment
- Code is automatically tested
