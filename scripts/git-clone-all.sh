#!/bin/bash

scriptPath=${0%/*}
. "$scriptPath/projects.sh"

#move to parent folder to clone other projects
pwd
cd ../..

cd vorderingenoverzicht || exit
for dir in $directories_vorderingenoverzicht; do
  git clone "git@gitlab.com:blauwe-knop/vorderingenoverzicht/$dir.git"
done
cd ..

mkdir -p common
cd common || exit
for dir in $directories_common; do
  git clone "git@gitlab.com:blauwe-knop/common/$dir.git"
done
cd ..

mkdir -p connect
cd connect || exit
for dir in $directories_connect; do
  git clone "git@gitlab.com:blauwe-knop/connect/$dir.git"
done
cd ..

mkdir -p tools
cd tools || exit
for dir in $directories_tools; do
  git clone "git@gitlab.com:blauwe-knop/tools/$dir.git"
done
cd ..

cd vorderingenoverzicht/development-environment || exit