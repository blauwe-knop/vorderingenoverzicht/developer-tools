#!/bin/bash
#copy /etc/hosts if /etc/hosts.original does not exist
sudo cp -n /etc/hosts /etc/hosts.original

cp /etc/hosts.original /tmp/newHosts

echo "#minikube ingress " >> /tmp/newHosts

MINIKUBE_IP="$(minikube ip)"
kubectl get ing -A -o=jsonpath="{range .items[*]}{\"${MINIKUBE_IP}\" }{'\t'}{.spec.rules[0].host}{'\n'}{end}" >> /tmp/newHosts

echo "#END minikube ingress " >> /tmp/newHosts
sudo cp /tmp/newHosts /etc/hosts