#!/bin/bash


scriptPath=${0%/*}
. "$scriptPath/projects.sh"
pwd
cd ../..

for dir in $directories_vorderingenoverzicht; do
  cd "vorderingenoverzicht/$dir" || exit
  pwd
  if [ -f "scripts/regenerate-gomock-files.sh" ]; then
    cd scripts || exit
    pwd
    sh "regenerate-gomock-files.sh"
    cd ..
  fi
  cd ../..
done

for dir in $directories_common; do
  cd "common/$dir" || exit
  pwd
  if [ -f "scripts/regenerate-gomock-files.sh" ]; then
    cd scripts || exit
    pwd
    sh "regenerate-gomock-files.sh"
    cd ..
  fi
  cd ../..
done

for dir in $directories_connect; do
  cd "connect/$dir" || exit
  pwd
  if [ -f "scripts/regenerate-gomock-files.sh" ]; then
    cd scripts || exit
    pwd
    sh "regenerate-gomock-files.sh"
    cd ..
  fi
  cd ../..
done

cd vorderingenoverzicht/development-environment