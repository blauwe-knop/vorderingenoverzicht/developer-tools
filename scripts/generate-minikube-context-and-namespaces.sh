#!/bin/bash
kubectl config use-context minikube

kubectl create namespace bk-cloud-native-pg-operator
kubectl create namespace bk-citizen
kubectl create namespace bk-scheme
kubectl create namespace bk-source-organization-manager
kubectl create namespace bk-app-manager-organization
kubectl create namespace bk-digid-organization
kubectl create namespace bk-tools
