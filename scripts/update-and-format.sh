#!/bin/bash


scriptPath=${0%/*}
. "$scriptPath/projects.sh"

pwd
cd ../..

for dir in $directories_vorderingenoverzicht; do
   cd "vorderingenoverzicht/$dir" || exit
   pwd
   if [ -f "go.mod" ]; then
     go get -u ./...
     go mod tidy
     golangci-lint run --fix
     goimports -w -local "gitlab.com/blauwe-knop/vorderingenoverzicht/$dir" .
   fi
   cd ../..
done

for dir in $directories_common; do
   cd "common/$dir" || exit
   pwd
   if [ -f "go.mod" ]; then
     go get -u ./...
     go mod tidy
     golangci-lint run --fix
     goimports -w -local "gitlab.com/blauwe-knop/common/$dir" .
   fi
   cd ../..
done

for dir in $directories_connect; do
   cd "connect/$dir" || exit
   pwd
   if [ -f "go.mod" ]; then
     go get -u ./...
     go mod tidy
     golangci-lint run --fix
     goimports -w -local "gitlab.com/blauwe-knop/connect/$dir" .
   fi
   cd ../..
done

for dir in $directories_tools; do
   cd "tools/$dir" || exit
   pwd
   if [ -f "go.mod" ]; then
     go get -u ./...
     go mod tidy
     golangci-lint run --fix
     goimports -w -local "gitlab.com/blauwe-knop/tools/$dir" .
   fi
   cd ../..
done

cd vorderingenoverzicht/development-environment || exit