#!/bin/bash


scriptPath=${0%/*}
. "$scriptPath/projects.sh"

# development-environment
pwd
git fetch --tags -f
git pull & git pull --tags
cd ../..

for dir in $directories_vorderingenoverzicht; do
  cd "vorderingenoverzicht/$dir" || exit
  pwd
  git fetch --tags -f
  git pull
  git pull --tags
  cd ../..
done

for dir in $directories_common; do
  cd "common/$dir" || exit
  pwd
  git fetch --tags -f
  git pull
  git pull --tags
  cd ../..
done

for dir in $directories_connect; do
  cd "connect/$dir" || exit
  pwd
  git fetch --tags -f
  git pull
  git pull --tags
  cd ../..
done

for dir in $directories_tools; do
  cd "tools/$dir" || exit
  pwd
  git fetch --tags -f
  git pull
  git pull --tags
  cd ../..
done

cd vorderingenoverzicht/development-environment || exit