#!/bin/bash

scriptPath=${0%/*}
. "$scriptPath/projects.sh"

pwd
cd ../..

for dir in $directories_vorderingenoverzicht; do
  cd "vorderingenoverzicht/$dir" || exit
  pwd
  if [ -d "api" ]; then
    docker run --rm -v "${PWD}/api:/api" openapitools/openapi-generator-cli:v7.9.0 generate \
        -i /api/openapi.yaml \
        -o /api \
        -g openapi
  fi
  cd ../..
done

for dir in $directories_common; do
  cd "common/$dir" || exit
  pwd
  if [ -d "api" ]; then
    docker run --rm -v "${PWD}/api:/api" openapitools/openapi-generator-cli:v7.9.0 generate \
        -i /api/openapi.yaml \
        -o /api \
        -g openapi
  fi
  cd ../..
done

for dir in $directories_connect; do
  cd "connect/$dir" || exit
  pwd
  if [ -d "api" ]; then
    docker run --rm -v "${PWD}/api:/api" openapitools/openapi-generator-cli:v7.9.0 generate \
        -i /api/openapi.yaml \
        -o /api \
        -g openapi
  fi
  cd ../..
done

for dir in $directories_tools; do
  cd "tools/$dir" || exit
  pwd
  if [ -d "api" ]; then
    docker run --rm -v "${PWD}/api:/api" openapitools/openapi-generator-cli:v7.9.0 generate \
        -i /api/openapi.yaml \
        -o /api \
        -g openapi
  fi
  cd ../..
done

cd vorderingenoverzicht/development-environment || exit