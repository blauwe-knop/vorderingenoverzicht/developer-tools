#!/bin/bash

echo https://registration-api.gemeente-tilburg.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://registration-api.gemeente-tilburg.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://registration-api.gemeente-tilburg.vorijk-demo.blauweknop.app/v1

echo https://session-api.gemeente-tilburg.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://session-api.gemeente-tilburg.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://session-api.gemeente-tilburg.vorijk-demo.blauweknop.app/v1

echo https://management-api.cjib.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://management-api.cjib.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://management-api.cjib.vorijk-demo.blauweknop.app/v1

echo https://financial-claim-request-api.cjib.vorijk-demo.blauweknop.app/v3
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://financial-claim-request-api.cjib.vorijk-demo.blauweknop.app/v3
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://financial-claim-request-api.cjib.vorijk-demo.blauweknop.app/v3

echo https://demo-simulation.cjib.vorijk-demo.blauweknop.app/v4
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://demo-simulation.cjib.vorijk-demo.blauweknop.app/v4
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://demo-simulation.cjib.vorijk-demo.blauweknop.app/v4

echo https://session-api.cjib.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://session-api.cjib.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://session-api.cjib.vorijk-demo.blauweknop.app/v1

echo https://api.stelsel.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://api.stelsel.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://api.stelsel.vorijk-demo.blauweknop.app/v1

echo https://management-api.stelsel.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate core https://management-api.stelsel.vorijk-demo.blauweknop.app/v1
docker run --rm registry.gitlab.com/commonground/don/adr-validator validate security-tls https://management-api.stelsel.vorijk-demo.blauweknop.app/v1
