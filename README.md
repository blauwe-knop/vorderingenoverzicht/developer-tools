# Development Environment Vorderingenoverzicht
This is the main developer documentation repository for the project 'Vorderingenoverzicht'.

To get started read the specific instructions for the OS you use for development:
- [DEVELOPER_LINUX.md](DEVELOPER_LINUX.md) for a Linux environment
- [DEVELOPER_MAC.md](DEVELOPER_MAC.md) for a Mac environment
General developer instructions are in [DEVELOPER.md](DEVELOPER.md).

Information on how to release a component is in [RELEASING.md](RELEASING.md)

The definition of done of the development team is in [DEFINITION_OF_DONE.md](DEFINITION_OF_DONE.md) 

Note: All other documentation is in the [docs](https://gitlab.com/blauwe-knop/vorderingenoverzicht/docs) repository
